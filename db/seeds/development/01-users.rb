module SeedUser
  def self.seed
    User.destroy_all

    # superuser
    user = FactoryGirl.create(:user, email: "user@example.com", password: 12345678)

    11.times do
      # build new user
      FactoryGirl.create(:user)
    end
  end
end
