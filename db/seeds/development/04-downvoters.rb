module SeedDownvoter
  def self.seed
    @pins = Pin.all.map(&:id)
    @users = User.all.map(&:id)

    10.times do
      @pin = Pin.find_by_id(@pins.sample)
      @user = User.find_by_id(@users.sample)
      @pin.downvote_by(@user)
    end
  end
end