module SeedPin
  def self.seed
    Pin.destroy_all
    
    12.times do
      random_image = Dir.entries(Rails.root.join('db', 'seeds', 'images'))
      random_image.delete('.')
      random_image.delete('..')
      random_image = random_image.sample
      random_image_path = Rails.root.join('db', 'seeds', 'images', random_image)

      # create product
      pin = FactoryGirl.build(:pin)
      pin.image = File.new(random_image_path, mode: 'r+')
      pin.save!
    end
  end
end
