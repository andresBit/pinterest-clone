# == Schema Information
#
# Table name: pins
#
#  id                 :integer          not null, primary key
#  title              :string(255)
#  description        :text
#  created_at         :datetime
#  updated_at         :datetime
#  user_id            :integer
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

FactoryGirl.define do
  factory :pin do
    title             { FFaker::Book::title }
    description       { FFaker::LoremJA::paragraph }
    created_at        { FFaker::Time::datetime }
    association       :user, factory: :user
  end
end
